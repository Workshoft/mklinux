![](resources/MKLINUX.png)

This repository hosts Mikel's Linux, my own Linux from Scratch.

LFS 8.3 can be downloaded here: http://www.linuxfromscratch.org/lfs/downloads/stable-systemd/LFS-BOOK-8.3-systemd.pdf

This is version 8.3-systemd of the Linux From Scratch book, dated September 1, 2018. If this book is more than six
months old, a newer and better version is probably already available. To find out, please check one of the mirrors via
http://www.linuxfromscratch.org/mirrors.html.

